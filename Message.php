<?php

namespace Pluto;




class Message
{
    protected $type;
    protected $code;
    protected $text;




    const TYPE_NONE = 0;
    const TYPE_DEFAULT = 1;
    const TYPE_NOTICE = 2;
    const TYPE_WARNING = 3;
    const TYPE_ERROR = 4;




    public function __construct($type = self::TYPE_NONE, $code = "", $text = "")
    {
        $this->type = $type;
        $this->code = $code;
        $this->text = $text;
    }




    public function Json()
    {
        return array
        (
            "type" => $this->Type(),
            "code" => $this->Code(),
            "text" => $this->Text()
        );
    }




    public function SetType($type)
    {
        $this->type = $type;
    }

    public function Type()
    {
        return $this->type;
    }

    public function SetCode($code)
    {
        $this->code = $code;
    }

    public function Code()
    {
        return $this->code;
    }

    public function SetText($text)
    {
        $this->text = $text;
    }

    public function Text()
    {
        return $this->text;
    }
}
