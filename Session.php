<?php

namespace Pluto;




class Session
{
    protected $name;
    protected $duration;
    protected $path;
    protected $domain;
    protected $secureOnly;
    protected $httpOnly;

    protected $data;




    public function __construct($name, $duration, $path, $domain, $secureOnly = FALSE, $httpOnly = TRUE)
    {
        $this->name = $name;
        $this->duration = $duration;
        $this->path = $path;
        $this->domain = $domain;
        $this->secureOnly = $secureOnly;
        $this->httpOnly = $httpOnly;

        $this->data = array();
    }




    public function Start()
    {
        session_set_cookie_params
        (
            $this->duration,
            $this->path,
            $this->domain,
            $this->secureOnly,
            $this->httpOnly
        );

        session_name($this->name);
        session_start();

        $this->data = $_SESSION;
    }

    public function Destroy()
    {
        setcookie
        (
            $this->name,
            "",
            time() - $this->duration,
            $this->path,
            $this->domain,
            $this->secureOnly,
            $this->httpOnly
        );

        session_destroy();
    }

    public function Regenerate()
    {
        session_regenerate_id(TRUE);
    }




    public function Data($key)
    {
        $value = NULL;

        if (isset($_SESSION[$key]))
        {
            $value = $_SESSION[$key];
        }

        return $value;
    }

    public function AddData($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function RemoveData($key)
    {
        unset($_SESSION[$key]);
    }
}
