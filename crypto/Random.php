<?php

namespace Pluto;




class Random
{
    protected function __construct()
    {
    }




    static public function CreateBytes($length)
    {
        $bytes = 0;
        $strong = FALSE;

        while ($strong === FALSE)
        {
            $bytes = openssl_random_pseudo_bytes($length, $strong);
        }

        return $bytes;
    }

    static public function CreateInteger($min, $max)
    {
        $bytes = \Pluto\Random::CreateBytes(4);
        $integer = bin2hex($bytes);
        $integer = hexdec($integer);
        $integer = $integer % ($max - $min);
        $integer = $integer + $min;
        return $integer;
    }

    static public function CreateFloat($min, $max)
    {
    }

    static public function CreateHexString($length)
    {
        $bytes = \Pluto\Random::CreateBytes($length / 2);
        return bin2hex($bytes);
    }
}
