<?php

namespace Pluto;




require_once(__DIR__ . "/Hash.php");




class SHA512Config extends \Pluto\Hash
{
    protected $rounds;




    public function __construct()
    {
        parent::__construct();

        $this->rounds = 0;
    }




    public function SetRounds($rounds)
    {
        $this->rounds = $rounds;
    }

    public function Rounds()
    {
        return $this->rounds;
    }
}




class SHA512 extends \Pluto\Hash
{
    protected $sha512Config;




    public function __construct(\Pluto\SHA512Config $sha512Config)
    {
        $this->sha512Config = $sha512Config;

        parent::__construct($sha512Config);
    }




    public function Create($plaintext)
    {
        $salt = "$6$rounds=" . $this->sha512Config->Rounds() . "$" . $this->sha512Config->Salt();
        return crypt($plaintext, $salt);
    }
}
