<?php

namespace Pluto;




require_once(__DIR__ . "/Random.php");




class HashInput
{
    protected $plaintext;
    protected $salt;
    protected $hashType;
    protected $cost;
    protected $hash;




    public function __construct()
    {
        $this->plaintext = "";
        $this->salt = "";
        $this->hashType = 0;
        $this->cost = 0;
        $this->hash = "";
    }




    public function SetPlaintext($plaintext)
    {
        $this->plaintext = $plaintext;
    }

    public function Plaintext()
    {
        return $this->plaintext;
    }

    public function SetSalt($salt)
    {
        $this->salt = $salt;
    }

    public function Salt()
    {
        return $this->salt;
    }

    public function SetHashType($hashType)
    {
        $this->hashType = $hashType;
    }

    public function HashType()
    {
        return $this->hashType;
    }

    public function SetCost($cost)
    {
        $this->cost = $cost;
    }

    public function Cost()
    {
        return $this->cost;
    }

    public function SetHash($hash)
    {
        $this->hash = $hash;
    }

    public function Hash()
    {
        return $this->hash;
    }
}




class Hash
{
    const HASH_TYPE_NONE = 0;
    const HASH_TYPE_DEFAULT = 1;
    const HASH_TYPE_BCRYPT = 2;
    const HASH_TYPE_SHA256 = 3;
    const HASH_TYPE_SHA512 = 4;




    protected function __construct()
    {
    }




    public static function Create(\Pluto\HashInput $hashInput)
    {
        switch ($hashInput->HashType())
        {
            case self::HASH_TYPE_BCRYPT:
            {
                self::CreateBcrypt($hashInput);
                break;
            }

            case self::HASH_TYPE_SHA256:
            {
                self::CreateSha256($hashInput);
                break;
            }

            case self::HASH_TYPE_SHA512:
            {
                self::CreateSha512($hashInput);
                break;
            }

            default:
            {
                break;
            }
        }

        return $hashInput->Hash();
    }




    public static function CreateSalt($hashType)
    {
        $salt = "";

        switch ($hashType)
        {
            case self::HASH_TYPE_BCRYPT:
            {
                $salt = self::CreateBcryptSalt();
                break;
            }

            case self::HASH_TYPE_SHA256:
            {
                $salt = self::CreateSha256Salt();
                break;
            }

            case self::HASH_TYPE_SHA512:
            {
                $salt = self::CreateSha512Salt();
                break;
            }

            default:
            {
                break;
            }
        }

        return $salt;
    }




    protected static function CreateBcrypt(\Pluto\HashInput $hashInput)
    {
        $salt = "$2y$" . $hashInput->Cost() . "$" . $hashInput->Salt();
        $hash = crypt($hashInput->Plaintext(), $salt);
        $hashInput->SetHash($hash);
    }

    protected static function CreateSha256(\Pluto\HashInput $hashInput)
    {
        return "";
    }

    protected static function CreateSha512(\Pluto\HashInput $hashInput)
    {
        return "";
    }




    protected static function CreateBcryptSalt()
    {
        return \Pluto\Random::CreateHexString(22);
    }

    protected static function CreateSha256Salt()
    {
    }

    protected static function CreateSha512Salt()
    {
    }
}
