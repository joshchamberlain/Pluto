<?php

namespace Pluto;




require_once(__DIR__ . "/Hash.php");




class BcryptConfig extends \Pluto\HashConfig
{
    public function __construct()
    {
        parent::__construct();
    }
}




class Bcrypt extends \Pluto\Hash
{
    protected $bcryptConfig;




    public function __construct(\Pluto\BcryptConfig $bcryptConfig)
    {
        $this->bcryptConfig = $bcryptConfig;

        parent::__construct($bcryptConfig);
    }




    public function Create($plaintext)
    {
        $salt = "$2y$" . $this->bcryptConfig->cost . "$" . $this->bcryptConfig->salt;
        return crypt($plaintext, $salt);
    }
}
