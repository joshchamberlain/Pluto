<?php

namespace Pluto;




class Validator
{
    public function __construct()
    {
    }




    public function ValidateString($string, $minLength, $maxLength = 0)
    {
        $result = 0;
        $length = mb_strlen($string);

        if ($minLength > 0 && $minLength > $length)
        {
            $result = 1;
        }

        else if ($maxLength > 0 && $maxLength < $length)
        {
            $result = 1;
        }

        return $result;
    }

    public function ValidateInt($value, $minValue = PHP_INT_MIN, $maxValue = PHP_INT_MAX)
    {
        $result = 0;
        
        if ($minValue > $value)
        {
            $result = 1;
        }
        
        else if ($maxValue < $value)
        {
            $result = 1;
        }

        return $result;
    }

    public function ValidateEmail($email)
    {
        $result = 0;

        if ($this->ValidateString($email, 6))
        {
            $result = 1;
        }

        else if (strpos($email, "@") === FALSE)
        {
            $result = 1;
        }

        else if (strpos($email, ".") === FALSE)
        {
            $result = 1;
        }

        return $result;
    }
}
