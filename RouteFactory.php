<?php

namespace Pluto;




require_once(__DIR__ . "/Route.php");




class RouteFactory
{
    public function __construct()
    {
    }




    public function Create(Array $path, \Pluto\ControllerFactory $controllerFactory)
    {
        return new \Pluto\Route($path, $controllerFactory);
    }
}
