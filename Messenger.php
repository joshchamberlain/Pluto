<?php

namespace Pluto;




require_once(__DIR__ . "/MessageContainerFactory.php");




abstract class Messenger
{
    protected $messageContainerFactory;
    protected $messageContainer;




    const ERROR_NONE = "Pluto::Messenger::ERROR_NONE";
    const ERROR_DEFAULT = "Pluto::Messenger::ERROR_DEFAULT";




    public function __construct()
    {
        $this->messageContainerFactory = new \Pluto\MessageContainerFactory();
        $this->messageContainer = $this->messageContainerFactory->Create();
    }
}
