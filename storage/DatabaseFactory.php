<?php

namespace Pluto;




require_once(__DIR__ . "/Database.php");




class DatabaseFactory
{
    public function __construct()
    {
    }




    public function Create($driver, $name, $host, $port, $user, $password)
    {
        return new \Pluto\Database($driver, $name, $host, $port, $user, $password);
    }
}
