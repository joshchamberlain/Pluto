<?php

namespace Pluto;




require_once(__DIR__ . "/DatabaseFactory.php");
require_once(__DIR__ . "/../Config.php");




class StaticDatabaseFactory extends \Pluto\DatabaseFactory
{
    static protected $db = NULL;




    public function __construct()
    {
        parent::__construct();
    }




    public function Create($driver = "", $name = "", $host = "", $port = "", $user = "", $password = "")
    {
        if ($driver === "")
        {
            $driver = \Pluto\Config::Data("database_driver");
        }

        if ($name === "")
        {
            $name = \Pluto\Config::Data("database_name");
        }

        if ($host === "")
        {
            $host = \Pluto\Config::Data("database_host");
        }

        if ($port === "")
        {
            $port = \Pluto\Config::Data("database_port");
        }

        if ($user === "")
        {
            $user = \Pluto\Config::Data("database_user");
        }

        if ($password === "")
        {
            $password = \Pluto\Config::Data("database_password");
        }

        if (self::$db === NULL)
        {
            self::$db = new \Pluto\Database
            (
                \Pluto\Config::Data("database_driver"),
                \Pluto\Config::Data("database_name"),
                \Pluto\Config::Data("database_host"),
                \Pluto\Config::Data("database_port"),
                \Pluto\Config::Data("database_user"),
                \Pluto\Config::Data("database_password")
            );
        }

        return self::$db;
    }
}
