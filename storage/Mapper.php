<?php

namespace Pluto;




require_once(__DIR__ . "/DatabaseFactory.php");
require_once(__DIR__ . "/../MessageContainerFactory.php");




abstract class Mapper
{
    protected $database;

    protected $table;

    protected $columns;
    protected $numColumns;
    
    protected $keys;
    protected $numKeys;

    protected $entityFactory;

    protected $messageContainerFactory;




    public function __construct(\Pluto\DatabaseFactory $databaseFactory = NULL)
    {
        if ($databaseFactory)
        {
            $this->database = $databaseFactory->Create();
        }

        else
        {
            $dbf = new \Pluto\StaticDatabaseFactory();
            $this->database = $dbf->Create();
        }

        $pdo = $this->database->PDO();
        $pdo->exec("SET NAMES utf8mb4");

        $this->table = "";

        $this->columns = array();
        $this->numColumns = 0;
    
        $this->keys = array();
        $this->numKeys = 0;

        $this->messageContainerFactory = new \Pluto\MessageContainerFactory();
    }




    public function CreateTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `" . $this->table . "`
        (
            " . implode(",", array_merge($this->columns, $this->keys)) . "
        ) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci";

        return $this->RawQuery($sql, array());
    }




    public function Create($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();

        if (is_object($o))
        {
            $messageContainer = $this->CreateObject($o);
        }

        else if (is_array($o))
        {
            $messageContainer = $this->CreateArray($o);
        }

        return $messageContainer;
    }

    public function CreateObject($o)
    {
        $methodNames = $this->GetMethodNames($o);
        $numMethodNames = sizeof($methodNames);

        $sql = $this->GenerateCreateSql();
        $values = array();

        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        for ($i = 1; $i < $numColumns; $i++)
        {
            $columnMethodName = ucfirst($columns[$i]);

            for ($j = 0; $j < $numMethodNames; $j++)
            {
                if ($columnMethodName === $methodNames[$j])
                {
                    $values[] = $o->$columnMethodName();
                    break;
                }
            }
        }

        $messageContainer = $this->RawQuery($sql, $values);

        if ($messageContainer->Result() === 0)
        {
            $o->SetId($this->database->LastInsertId());
        }

        return $messageContainer;
    }

    public function CreateArray($o)
    {
        $sql = $this->GenerateCreateSql();
        $values = array();

        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        for ($i = 1; $i < $numColumns; $i++)
        {
            if (array_key_exists($columns[$i], $o))
            {
                $values[] = $o[$columns[$i]];
            }
        }

        $messageContainer = $this->RawQuery($sql, $values);

        if ($messageContainer->Result() === 0)
        {
            $o["id"] = $this->database->LastInsertId();
        }

        return $messageContainer;
    }

    public function Update($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();

        if (is_object($o))
        {
            $messageContainer = $this->UpdateObject($o);
        }

        else if (is_array($o))
        {
            $messageContainer = $this->UpdateArray($o);
        }

        return $messageContainer;
    }

    public function UpdateObject($o)
    {
        $methodNames = $this->GetMethodNames($o);
        $numMethodNames = sizeof($methodNames);

        $sql = $this->GenerateUpdateSql();
        $values = array();

        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        for ($i = 1; $i < $numColumns; $i++)
        {
            $columnMethodName = ucfirst($columns[$i]);

            for ($j = 0; $j < $numMethodNames; $j++)
            {
                if ($columnMethodName === $methodNames[$j])
                {
                    $values[] = $o->$columnMethodName();
                    break;
                }
            }
        }

        $values[] = $o->Id();

        return $this->RawQuery($sql, $values);
    }

    public function UpdateArray($o)
    {
        $sql = $this->GenerateUpdateSql();
        $values = array();

        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        for ($i = 1; $i < $numColumns; $i++)
        {
            if (array_key_exists($columns[$i], $o))
            {
                $values[] = $o[$columns[$i]];
            }
        }

        if (array_key_exists("id", $o))
        {
            $values[] = $o["id"];
        }

        return $this->RawQuery($sql, $values);
    }

    public function Delete($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();

        if (is_object($o))
        {
            $messageContainer = $this->DeleteObject($o);
        }

        else if (is_array($o))
        {
            $messageContainer = $this->DeleteArray($o);
        }

        return $messageContainer;
    }

    public function DeleteObject($o)
    {
        $sql = $this->GenerateDeleteSql();
        $values = array($o->Id());
        return $this->RawQuery($sql, $values);
    }

    public function DeleteArray($o)
    {
        $sql = $this->GenerateDeleteSql();
        $values = array();

        if (array_key_exists("id", $o))
        {
            $values[] = $o["id"];
        }

        return $this->RawQuery($sql, $values);
    }

    public function Read($clause, Array $vars, Array &$data)
    {
        $sql = $this->GenerateReadSql($clause);
        return $this->RawQuery($sql, $vars, $data);
    }

    public function RawQuery($sql, Array $vars, Array &$data = NULL)
    {
        $messageContainer = $this->messageContainerFactory->Create();
        $messageContainer->SetResult(1);

        $database = $this->database;

        if ($database)
        {
            $result = 1;

            try
            {
                $result = $database->Prepare($sql, $vars, $data);
            }

            catch (\Exception $e)
            {
                $messageContainer->AddErrorMessage
                (
                    "Pluto::Mapper::ERROR_RAW_QUERY",
                    $e->getMessage()
                );

                $result = 1;
            }

            $messageContainer->SetResult($result);
        }

        return $messageContainer;
    }




    public function FindById($id)
    {
        return $this->FindSingle
        (
            "WHERE `id`=? LIMIT 1",
            array($id)
        );
    }

    public function FindByCode($code)
    {
        return $this->FindSingle
        (
            "WHERE BINARY `code`=? LIMIT 1",
            array($code)
        );
    }

    public function FindAll()
    {
        return $this->FindMultiple
        (
            "ORDER BY `id` ASC",
            array()
        );
    }




    public function SetDatabase($database)
    {
        $this->database = $database;
    }

    public function Database()
    {
        return $this->database;
    }

    public function SetTable($table)
    {
        $this->table = $table;
    }

    public function Table()
    {
        return $this->table;
    }

    public function Columns()
    {
        return $this->columns;
    }

    public function NumColumns()
    {
        return $this->numColumns;
    }





    public function CleanColumns($columns)
    {
        $cleanColumns = array();
        
        foreach ($columns as $column)
        {
            $startPos = 1;
            $endPos = strpos($column, "`", $startPos);
            
            if ($startPos !== FALSE && $endPos !== FALSE)
            {
                $cleanColumns[] = substr($column, 1, $endPos - $startPos);
            }
        }

        return $cleanColumns;
    }

    public function PrefixedColumns(Array $columns, $tableAlias = "", $prefix = "", $aggregateFunc = "")
    {
        $prefixedColumns = array();

        $cleanColumns = $this->CleanColumns($columns);

        foreach ($cleanColumns as $cleanColumn)
        {
            $prefixedColumn = "`" . $cleanColumn . "`";

            if (strlen(trim($tableAlias)))
            {
                $prefixedColumn = "`" . $tableAlias . "`." . $prefixedColumn;
            }

            if (strlen(trim($aggregateFunc)))
            {
                $prefixedColumn = $aggregateFunc . "(" . $prefixedColumn . ")";
            }
        
            if (strlen(trim($prefix)))
            {
                $prefixedColumn = $prefixedColumn . " AS `" . $prefix . "_" . $cleanColumn . "`";
            }

            else
            {
                $prefixedColumn = $prefixedColumn . " AS `" . $cleanColumn . "`";
            }

            $prefixedColumns[] = $prefixedColumn;
        }

        return $prefixedColumns;
    }




    protected function GetMethodNames($o)
    {
        $reflect = new \ReflectionClass($o);
        $methods = $reflect->getMethods(\ReflectionProperty::IS_PUBLIC);
        $numMethods = sizeof($methods);
        $methodNames = array();

        for ($i = 0; $i < $numMethods; $i++)
        {
            $methodNames[$i] = $methods[$i]->getName();
        }

        return $methodNames;
    }




    protected function GenerateCreateSql()
    {
        $table = $this->table;
        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        $sql = "INSERT INTO `" . $table . "` (";

        for ($i = 1; $i < $numColumns; $i++)
        {
            if ($i > 1)
            {
                $sql = $sql . ",";
            }

            $sql = $sql . "`" . $columns[$i] . "`";
        }

        $sql = $sql . ") VALUES (";

        for ($i = 1; $i < $numColumns; $i++)
        {
            if ($i > 1)
            {
                $sql = $sql . ",";
            }

            $sql = $sql . "?";
        }

        $sql = $sql . ")";

        return $sql;
    }

    protected function GenerateUpdateSql()
    {
        $table = $this->table;
        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        $sql = "UPDATE `" . $table . "` SET ";

        for ($i = 1; $i < $numColumns; $i++)
        {
            if ($i > 1)
            {
                $sql = $sql . ",";
            }

            $sql = $sql . "`" . $columns[$i] . "`=?";
        }

        $sql = $sql . " WHERE `id`=? LIMIT 1";

        return $sql;
    }

    protected function GenerateDeleteSql()
    {
        $sql = "DELETE FROM `" . $this->table . "` WHERE `id`=? LIMIT 1";
        return $sql;
    }

    protected function GenerateReadSql($clause)
    {
        $table = $this->table;
        $columns = $this->ProcessedColumns($this->columns);
        $numColumns = sizeof($columns);

        $sql = "SELECT ";

        for ($i = 0; $i < $numColumns; $i++)
        {
            if ($i > 0)
            {
                $sql = $sql . ",";
            }

            $sql = $sql . "`" . $columns[$i] . "`";
        }

        $sql = $sql . " FROM `" . $table . "` " . $clause;

        return $sql;
    }




    public function FindSingle($clause, Array $vars)
    {
        $o = NULL;

        $data = array();
        $messageContainer = $this->Read($clause, $vars, $data);

        if ($messageContainer->Result() === 0 && sizeof($data))
        {
            $o = $this->BuildObject($data[0]);
        }

        return $o;
    }

    public function FindMultiple($clause, Array $vars)
    {
        $o = NULL;

        $data = array();
        $messageContainer = $this->Read($clause, $vars, $data);

        if ($messageContainer->Result() === 0)
        {
            $time1 = microtime(TRUE);
            $o = $this->BuildObjects($data);
            $time2 = microtime(TRUE);
            //echo "time to build objects: " . ($time2 - $time1) . " seconds\n";
        }

        return $o;
    }

    //abstract protected function BuildObject(Array $data);
    //abstract protected function PopulateObject(&$o, Array $data);

    protected function BuildObject(Array $data)
    {
        $o = $this->entityFactory ? $this->entityFactory->Create() : NULL;
        $this->PopulateObject($o, $data);
        return $o;
    }

    protected function PopulateObject(&$o, Array $data)
    {
        $reflect = new \ReflectionClass($o);
        
        foreach ($data as $k => $v)
        {
            $setter = "Set" . ucwords($k);

            if ($reflect->hasMethod($setter))
            {
                $o->$setter($v);
            }
        }
    }




    protected function BuildObjects(Array $data)
    {
        $o = array();

        $rows = $data;
        $numRows = sizeof($rows);

        for ($i = 0; $i < $numRows; $i++)
        {
            $o[$i] = $this->BuildObject($rows[$i]);
        }

        return $o;
    }




    protected function ProcessedColumns($columns)
    {
        $processedColumns = array();

        $numColumns = sizeof($columns);

        if ($numColumns && strpos($columns[0], "`") !== FALSE)
        {
            for ($i = 0; $i < $numColumns; $i++)
            {
                $startPos = 1;
                $endPos = strpos($columns[$i], "`", $startPos);

                if ($endPos !== FALSE)
                {
                    $processedColumns[] = substr($columns[$i], $startPos, $endPos - $startPos);
                }
            }
        }

        else
        {
            for ($i = 0; $i < $numColumns; $i++)
            {
                $processedColumns[] = $columns[$i];
            }
        }

        return $processedColumns;
    }
}
