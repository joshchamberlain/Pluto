<?php

namespace Pluto;




class Database
{
    protected $driver;
    protected $name;
    protected $host;
    protected $port;
    protected $user;
    protected $password;

    protected $pdo;
    protected $activeTransaction;




    public function __construct($driver, $name, $host, $port, $user, $password)
    {
        $this->driver = $driver;
        $this->name = $name;
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;

        $dsn = $this->driver . ":";
        $dsn = $dsn . "dbname=" . $this->name . ";";
        $dsn = $dsn . "host=" . $this->host . ";";
        $dsn = $dsn . "port=" . $this->port . ";";
        $dsn = $dsn . "charset=utf8";
    
        try
        {
            $pdoOptions = array();
            
            if (\Pluto\Config::Data("mysql_attr_ssl_key") !== NULL)
            {
                $pdoOptions[\PDO::MYSQL_ATTR_SSL_KEY] = \Pluto\Config::Data("mysql_attr_ssl_key");
            }

            if (\Pluto\Config::Data("mysql_attr_ssl_cert") !== NULL)
            {
                $pdoOptions[\PDO::MYSQL_ATTR_SSL_CERT] = \Pluto\Config::Data("mysql_attr_ssl_cert");
            }

            if (\Pluto\Config::Data("mysql_attr_ssl_ca") !== NULL)
            {
                $pdoOptions[\PDO::MYSQL_ATTR_SSL_CA] = \Pluto\Config::Data("mysql_attr_ssl_ca");
            }

            $pdoOptions[\PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT] = 0;
            
            $this->pdo = new \PDO($dsn, $this->user, $this->password, $pdoOptions);
            $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, FALSE);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        catch (\PDOException $e)
        {
            echo "PDO Error: " . $e->getMessage() . "<br />";
        }

        $this->activeTransaction = 0;
    }




    public function StartTransaction()
    {
        $result = 0;

        if ($this->activeTransaction === 0)
        {
            $this->activeTransaction = 1;
            $this->pdo->beginTransaction();
            $result = 1;
        }

        return $result;
    }

    public function Rollback($transactionStarted = 1)
    {
        if ($transactionStarted)
        {
            $this->pdo->rollBack();
            $this->activeTransaction = 0;
        }
    }

    public function Commit($transactionStarted = 1)
    {
        if ($transactionStarted)
        {
            $this->pdo->commit();
            $this->activeTransaction = 0;
        }
    }

    public function Prepare($sql, Array $vars, Array &$data = NULL)
    {
        $stmt = $this->pdo->prepare($sql);
        $numVars = sizeof($vars);

        for ($i = 0; $i < $numVars; $i++)
        {
            $stmt->bindValue($i + 1, $vars[$i]);
        }

        $result = 1;

        if ($stmt->execute())
        {
            $result = 0;
        }

        if ($data !== NULL && $result === 0)
        {
            $time1 = microtime(TRUE);

            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            /*
            for ($i = 0; $row = $stmt->fetch(\PDO::FETCH_ASSOC, \PDO::FETCH_ORI_NEXT); $i++)
            {
                $data[$i] = $row;
            }
            */

            $time2 = microtime(TRUE);

            //echo "fetching took " . ($time2 - $time1) . " seconds\n";
        }

        return $result;
    }

    public function LastInsertId()
    {
        return $this->pdo->lastInsertId();
    }





    public function PDO()
    {
        return $this->pdo;
    }
}
