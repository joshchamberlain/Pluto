<?php

namespace Pluto;




require_once(__DIR__ . "/Message.php");




class MessageContainer
{
    protected $result;
    protected $messages;
    protected $numMessages;




    public function __construct()
    {
        $this->result = 255;
        $this->messages = array();
        $this->numMessages = 0;
    }




    public function Reset()
    {
        $this->result = 255;
        $this->messages = array();
        $this->numMessages = 0;
    }

    public function Append(\Pluto\MessageContainer $messageContainer)
    {
        $messages = $messageContainer->Messages();
        $numMessages = $messageContainer->NumMessages();

        for ($i = 0; $i < $numMessages; $i++)
        {
            $this->AddRawMessage($messages[$i]);
        }
    }

    public function AddRawMessage(\Pluto\Message $message)
    {
        $this->messages[$this->numMessages] = $message;
        $this->numMessages = $this->numMessages + 1;
        return 0;
    }

    public function AddRawMessageClone(\Pluto\Message $m)
    {
        $message = new \Pluto\Message($m->Type(), $m->Code(), $m->Text());
        return $this->AddRawMessage($message);
    }

    public function AddMessage($type, $code, $text = "")
    {
        $message = new \Pluto\Message($type, $code, $text);
        return $this->AddRawMessage($message);
    }

    public function AddNoticeMessage($code, $text = "")
    {
        return $this->AddMessage(\Pluto\Message::TYPE_NOTICE, $code, $text);
    }

    public function AddWarningMessage($code, $text = "")
    {
        return $this->AddMessage(\Pluto\Message::TYPE_WARNING, $code, $text);
    }

    public function AddErrorMessage($code, $text = "")
    {
        return $this->AddMessage(\Pluto\Message::TYPE_ERROR, $code, $text);
    }




    public function Json()
    {
        $messages = $this->Messages();
        $numMessages = $this->NumMessages();

        $noticeMessages = $this->NoticeMessages();
        $numNoticeMessages = $this->NumNoticeMessages();

        $warningMessages = $this->WarningMessages();
        $numWarningMessages = $this->NumWarningMessages();

        $errorMessages = $this->ErrorMessages();
        $numErrorMessages = $this->NumErrorMessages();

        $a = array();

        $a["messages"] = array();
        $a["numMessages"] = $numMessages;

        $a["noticeMessages"] = array();
        $a["numNoticeMessages"] = $numNoticeMessages;

        $a["warningMessages"] = array();
        $a["numWarningMessages"] = $numWarningMessages;

        $a["errorMessages"] = array();
        $a["numErrorMessages"] = $numErrorMessages;

        for ($i = 0; $i < $numMessages; $i++)
        {
            $a["messages"][] = $messages[$i]->Json();
        }

        for ($i = 0; $i < $numNoticeMessages; $i++)
        {
            $a["noticeMessages"][] = $noticeMessages[$i]->Json();
        }

        for ($i = 0; $i < $numWarningMessages; $i++)
        {
            $a["warningMessages"][] = $warningMessages[$i]->Json();
        }

        for ($i = 0; $i < $numErrorMessages; $i++)
        {
            $a["errorMessages"][] = $errorMessages[$i]->Json();
        }

        return $a;
    }




    public function Process(Array $messageCodeTexts)
    {
        $mc = new \Pluto\MessageContainer();

        for ($i = 0; $i < $this->numMessages; $i++)
        {
            if ($messageCodeTexts && array_key_exists($this->messages[$i]->Code(), $messageCodeTexts))
            {
                $this->messages[$i]->SetText($messageCodeTexts[$this->messages[$i]->Code()]);
            }

            $mc->AddRawMessage($this->messages[$i]);
        }

        return $mc;
    }




    public function SetResult($result)
    {
        $this->result = $result;
    }

    public function Result()
    {
        return $this->result;
    }

    public function Message($position)
    {
        $message = NULL;

        if (is_int($position) && $position >= 0 && $position < $this->numMessages)
        {
            $message = $this->messages[$position];
        }

        return $message;
    }

    public function MessageByCode($code)
    {
        $message = NULL;

        for ($i = 0; $i < $this->numMessages; $i++)
        {
            if ($this->messages[$i]->Code() === $code)
            {
                $message = $this->messages[$i];
                break;
            }
        }

        return $message;
    }

    public function Messages($type = \Pluto\Message::TYPE_NONE)
    {
        $messages = array();

        for ($i = 0; $i < $this->numMessages; $i++)
        {
            if ($type !== \Pluto\Message::TYPE_NONE && $type !== \Pluto\Message::TYPE_DEFAULT)
            {
                if ($type === $this->messages[$i]->Type())
                {
                    $messages[] = $this->messages[$i];
                }
            }
    
            else
            {
                $messages[] = $this->messages[$i];
            }
        }

        return $messages;
    }

    public function NumMessages($type = \Pluto\Message::TYPE_NONE)
    {
        $numMessages = 0;

        if ($type !== \Pluto\Message::TYPE_NONE && $type !== \Pluto\Message::TYPE_DEFAULT)
        {
            for ($i = 0; $i < $this->numMessages; $i++)
            {
                if ($type === $this->messages[$i]->Type())
                {
                    $numMessages = $numMessages + 1;
                }
            }
        }

        else
        {
            $numMessages = $this->numMessages;
        }

        return $numMessages;
    }

    public function NoticeMessages()
    {
        return $this->Messages(\Pluto\Message::TYPE_NOTICE);
    }

    public function NumNoticeMessages()
    {
        return $this->NumMessages(\Pluto\Message::TYPE_NOTICE);
    }

    public function WarningMessages()
    {
        return $this->Messages(\Pluto\Message::TYPE_WARNING);
    }

    public function NumWarningMessages()
    {
        return $this->NumMessages(\Pluto\Message::TYPE_WARNING);
    }

    public function ErrorMessages()
    {
        return $this->Messages(\Pluto\Message::TYPE_ERROR);
    }

    public function NumErrorMessages()
    {
        return $this->NumMessages(\Pluto\Message::TYPE_ERROR);
    }
}
