<?php

namespace Pluto;




require_once(__DIR__ . "/Messenger.php");
require_once(__DIR__ . "/ValidatorFactory.php");
require_once(__DIR__ . "/storage/MapperFactory.php");




abstract class Service extends \Pluto\Messenger
{
    protected $validator;
    protected $mapper;




    const ERROR_VALIDATION = "Pluto::Service::ERROR_VALIDATION";
    const ERROR_MAPPER = "Pluto::Service::ERROR_MAPPER";

    const ERROR_DEFAULT = "Pluto::Service::ERROR_DEFAULT";
    const ERROR_ID = "Pluto::Service::ERROR_ID";
    const ERROR_CODE = "Pluto::Service::ERROR_CODE";




    public function __construct(\Pluto\ValidatorFactory $validatorFactory, \Pluto\MapperFactory $mapperFactory)
    {
        parent::__construct();

        $this->validator = $validatorFactory->Create();
        $this->mapper = $mapperFactory->Create();

        $this->messageContainer->AddErrorMessage(self::ERROR_VALIDATION, "Errors occurred during validation.");
        $this->messageContainer->AddErrorMessage(self::ERROR_MAPPER, "Errors occurred during the save process.");
        
        $this->messageContainer->AddErrorMessage(self::ERROR_DEFAULT, "The object provided was invalid or the error could not be determined.");
        $this->messageContainer->AddErrorMessage(self::ERROR_ID, "The ID provided was invalid.");
        $this->messageContainer->AddErrorMessage(self::ERROR_CODE, "The code provided was invalid.");
    }




    public function __call($name, $arguments)
    {
        if (strpos($name, "Find") !== FALSE)
        {
            $o = NULL;

            if ($this->mapper)
            {
                $o = call_user_func_array(array($this->mapper, $name), $arguments);
                $o = $this->PopulateEntities($o);
                return $o;
            }
        }

        throw new \Exception("Call failed in Pluto::Service.");
    }




    public function Create($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();
        $messageContainer->SetResult(1);

        $validateMessageContainer = $this->Validate($o);

        if ($validateMessageContainer->Result() === 1)
        {
            $messageContainer->Append($validateMessageContainer);
            return $messageContainer;
        }

        $mapperMessageContainer = $this->mapper->Create($o);

        if ($mapperMessageContainer->Result() === 1)
        {
            $messageContainer->AddRawMessageClone($this->messageContainer->MessageByCode(self::ERROR_MAPPER));
            $messageContainer->Append($mapperMessageContainer);
            return $messageContainer;
        }

        $messageContainer->SetResult(0);
        return $messageContainer;
    }

    public function Update($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();
        $messageContainer->SetResult(1);

        $validateMessageContainer = $this->Validate($o);

        if ($validateMessageContainer->Result() === 1)
        {
            $messageContainer->Append($validateMessageContainer);
            return $messageContainer;
        }

        $mapperMessageContainer = $this->mapper->Update($o);

        if ($mapperMessageContainer->Result() === 1)
        {
            $messageContainer->AddRawMessageClone($this->messageContainer->MessageByCode(self::ERROR_MAPPER));
            $messageContainer->Append($mapperMessageContainer);
            return $messageContainer;
        }

        $messageContainer->SetResult(0);
        return $messageContainer;
    }

    public function Delete($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();
        $messageContainer->SetResult(1);

        $mapperMessageContainer = $this->mapper->Delete($o);

        if ($mapperMessageContainer->Result() === 1)
        {
            $messageContainer->AddRawMessageClone($this->messageContainer->MessageByCode(self::ERROR_MAPPER));
            $messageContainer->Append($mapperMessageContainer);
            return $messageContainer;
        }

        $messageContainer->SetResult(0);
        return $messageContainer;
    }




    public function Validate($o)
    {
        $messageContainer = $this->messageContainerFactory->Create();
        $messageContainer->SetResult(0);

        if ($o === NULL)
        {
            $messageContainer->AddRawMessageClone($this->messageContainer->MessageByCode(self::ERROR_DEFAULT));
            $messageContainer->SetResult(1);
            return $messageContainer;
        }

        if ($this->validator->ValidateInt($o->Id(), 0))
        {
            $messageContainer->AddRawMessageClone($this->messageContainer->MessageByCode(self::ERROR_ID));
        }

        if ($this->validator->ValidateString($o->Code(), 1))
        {
            $messageContainer->AddRawMessageClone($this->messageContainer->MessageByCode(self::ERROR_CODE));
        }

        if ($messageContainer->NumErrorMessages())
        {
            $messageContainer->SetResult(1);
        }

        return $messageContainer;
    }




    public function PopulateEntity(&$o)
    {
        return $o;
    }

    public function PopulateEntities(&$o)
    {
        if (is_array($o))
        {
            $numEntities = sizeof($o);
        
            for ($i = 0; $i < $numEntities; $i++)
            {
                $this->PopulateEntity($o[$i]);
            }
        }

        else if ($o)
        {
            $this->PopulateEntity($o);
        }

        return $o;
    }
}
