<?php

namespace Pluto;




class HttpResponse
{
    protected $statusCode = NULL;
    protected $headers = array();
    protected $views = array();
    protected $redirected = 0;




    public function __construct()
    {
    }




    public function Send()
    {
        if ($this->statusCode !== NULL)
        {
            $this->AddHeader("HTTP/1.1 " . $this->statusCode);
        }

        for ($i = 0; $i < sizeof($this->headers); $i++)
        {
            header($this->headers[$i]);
        }

        if ($this->redirected === 0)
        {
            for ($i = 0; $i < sizeof($this->views); $i++)
            {
                $this->views[$i]->Render();
            }
        }
    }




    public function AddHeader($header)
    {
        $this->headers[] = $header;
    }

    public function AddView(\Pluto\View $view)
    {
        $this->views[] = $view;
    }

    public function Redirect($url)
    {
        $this->redirected = 1;
        $this->AddHeader("Location: " . $url);
    }

    public function SetStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        $this->AddHeader("HTTP/1.1 " . $code);
    }

    public function StatusCode()
    {
        return $this->statusCode;
    }

    public function AddCookie($name, $value, $domain = NULL, $expires = -1, $secure = TRUE, $httpOnly = TRUE)
    {
        $c = array();
        $c[] = $name . "=" . $value . "";

        if ($domain)
        {
            $c[] = "Domain=" . $domain;
        }

        $c[] = "Path=/";

        $expires = $expires === -1 ? (time() + (60 * 60 * 24 * 365)) : $expires;

        if ($expires !== NULL)
        {
            $c[] = "Expires=" . date("D, d M Y H:i:s", $expires) . " GMT";
        }

        if ($secure)
        {
            $c[] = "Secure";
        }

        if ($httpOnly)
        {
            $c[] = "HttpOnly";
        }

        $this->AddHeader("Set-Cookie: " . implode("; ", $c));
    }
}
