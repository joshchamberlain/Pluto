<?php

namespace Pluto;




require_once(__DIR__ . "/Route.php");
require_once(__DIR__ . "/HttpRequest.php");
require_once(__DIR__ . "/HttpResponse.php");




class Router
{
    protected $routes;
    protected $numRoutes;

    protected $pathVarNames;
    protected $numPathVarNames;




    public function __construct()
    {
        $this->routes = array();
        $this->numRoutes = 0;

        $this->pathVarNames = array();
        $this->numPathVarNames = 0;
    }




    public function RegisterRoute(\Pluto\Route $route)
    {
        $this->routes[$this->numRoutes] = $route;
        $this->numRoutes = $this->numRoutes + 1;
        return 0;
    }

    public function RegisterRoutes(Array $routes)
    {
        $numRoutes = sizeof($routes);

        for ($i = 0; $i < $numRoutes; $i++)
        {
            $this->RegisterRoute($routes[$i]);
        }

        return 0;
    }

    public function UnregisterRoute(\Pluto\Route $route)
    {
        $unregistered = 1;

        for ($i = 0; $i < $this->numRoutes; $i++)
        {
            if ($this->routes[$i] === $route || $this->routes[$i]->ComparePath($route->Path()) === 0)
            {
                $this->routes = array_splice($this->routes, $i, 1);
                $this->numRoutes = $this->numRoutes - 1;
                $unregistered = 0;
            }
        }

        return $unregistered;
    }




    public function RegisterPathVarName($pathVarName)
    {
        $this->pathVarNames[] = $pathVarName;
        $this->numPathVarNames = $this->numPathVarNames + 1;
    }

    public function RegisterPathVarNames(Array $pathVarNames)
    {
        $this->pathVarNames = $pathVarNames;
        $this->numPathVarNames = sizeof($this->pathVarNames);
    }

    public function UnregisterPathVarName($pathVarName)
    {
        $unregistered = 1;

        for ($i = 0; $i < $this->numPathVarNames; $i++)
        {
            if ($this->pathVarNames[$i] === $pathVarName)
            {
                $this->pathVarNames = array_splice($this->pathVarNames, $i, 1);
                $this->numPathVarNames = $this->numPathVarNames - 1;
                $unregistered = 0;
            }
        }

        return $unregistered;
    }




    public function ProcessHttpRequest(\Pluto\HttpRequest $httpRequest)
    {
        $httpResponse = new \Pluto\HttpResponse();

        $path = $this->BuildPathFromHttpRequest($httpRequest);
        $route = $this->FindRoute($path);

        if ($route)
        {
            $httpResponse = $route->ProcessHttpRequest($httpRequest);
        }

        return $httpResponse;
    }




    protected function BuildPathFromHttpRequest(\Pluto\HttpRequest $httpRequest)
    {
        $path = array();

        for ($i = 0; $i < $this->numPathVarNames; $i++)
        {
            if ($httpRequest->GetData($this->pathVarNames[$i]))
            {
                $path[] = $httpRequest->GetData($this->pathVarNames[$i]);
            }
        }

        return $path;
    }

    protected function FindRoute(Array $path)
    {
        $route = NULL;

        for ($i = 0; $i < $this->numRoutes; $i++)
        {
            if ($this->routes[$i]->ComparePath($path) === 0)
            {
                $route = $this->routes[$i];
                break;
            }
        }

        return $route;
    }
}
