<?php

namespace Pluto;




class Config
{
    static protected $data = array();




    protected function __construct()
    {
    }




    static public function AddData($key, $value)
    {
        self::$data[$key] = $value;
    }

    static public function Data($key)
    {
        if (array_key_exists($key, self::$data) === TRUE)
        {
            return self::$data[$key];
        }

        return NULL;
    }
}
