<?php

namespace Pluto;




require_once(__DIR__ . "/HttpRequest.php");
require_once(__DIR__ . "/ControllerFactory.php");




class Route
{
    protected $path;
    protected $numPaths;
    protected $controllerFactory;




    public function __construct(Array $path, \Pluto\ControllerFactory $controllerFactory)
    {
        $this->path = $path;
        $this->numPaths = sizeof($this->path);
        $this->controllerFactory = $controllerFactory;
    }




    public function ProcessHttpRequest(\Pluto\HttpRequest $httpRequest)
    {
        $httpResponse = NULL;
        $controller = $this->controllerFactory->Create();

        if ($controller)
        {
            $httpResponse = $controller->ProcessHttpRequest($httpRequest);
        }

        return $httpResponse;
    }




    public function ComparePath(Array $path)
    {
        $result = 0;

        $numPaths = sizeof($path);

        if ($numPaths === $this->numPaths)
        {
            for ($i = 0; $i < $this->numPaths; $i++)
            {
                if ($this->path[$i] !== "*")
                {
                    if ($this->path[$i] !== $path[$i])
                    {
                        $result = 1;
                        break;
                    }
                }
            }
        }

        else
        {
            $result = 1;
        }

        return $result;
    }




    public function Path()
    {
        return $this->path;
    }

    public function NumPaths()
    {
        return $this->numPaths;
    }
}
