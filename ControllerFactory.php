<?php

namespace Pluto;




require_once(__DIR__ . "/Controller.php");




abstract class ControllerFactory
{
    public function __construct()
    {
    }




    abstract public function Create();
}
