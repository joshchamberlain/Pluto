<?php

namespace Pluto;




require_once(__DIR__ . "/HttpRequest.php");
require_once(__DIR__ . "/HttpResponse.php");




abstract class Controller
{
    protected $view;
    protected $httpResponse;




    public function __construct()
    {
        $this->view = NULL;
        $this->httpResponse = NULL;
    }




    public function ProcessHttpRequest(\Pluto\HttpRequest $httpRequest)
    {
        $httpResponse = $this->Run($httpRequest);
        return $httpResponse;
    }




    abstract public function Run(\Pluto\HttpRequest $httpRequest);




    protected function GenerateCsrf(\Pluto\HttpRequest $httpRequest, $tokenName)
    {
        $bytes = \Pluto\Random::CreateHexString(32);
        $httpRequest->Session()->AddData($tokenName, $bytes);
        return $bytes;
    }

    protected function CheckCsrf(\Pluto\HttpRequest $httpRequest, $tokenName)
    {
        $result = 0;

        $posted = $httpRequest->PostData($tokenName);
        $stored = $httpRequest->Session()->Data($tokenName);

        if ($posted === $stored)
        {
            $result = 1;
        }

        return $result;
    }
}
