<?php

namespace Pluto;




require_once(__DIR__ . "/Session.php");




class HttpRequest
{
    protected $envData;
    protected $getData;
    protected $postData;
    protected $session;




    public function __construct(Array $envData, Array $getData, Array $postData, $session)
    {
        $this->envData = $envData;
        $this->getData = $getData;
        $this->postData = $postData;
        $this->session = $session;
    }




    public function Method()
    {
        return $this->EnvData("REQUEST_METHOD");
    }

    public function Uri()
    {
        return $this->EnvData("REQUEST_URI");
    }

    public function Body()
    {
        return http_get_request_body();
    }

    public function RemoteAddress()
    {
        return $this->EnvData("REMOTE_ADDR");
    }

    public function EnvData($key)
    {
        $value = NULL;
        
        if (isset($this->envData[$key]))
        {
            $value = $this->envData[$key];
        }

        return $value;
    }

    public function GetData($key)
    {
        $value = NULL;

        if (isset($this->getData[$key]))
        {
            $value = $this->getData[$key];
        }

        return $value;
    }

    public function PostData($key)
    {
        $value = NULL;

        if (isset($this->postData[$key]))
        {
            $value = $this->postData[$key];
        }

        return $value;
    }

    public function Session()
    {
        return $this->session;
    }

    public function SessionData($key)
    {
        return $this->session->Data($key);
    }
}
