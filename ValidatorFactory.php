<?php

namespace Pluto;




require_once(__DIR__ . "/Validator.php");




class ValidatorFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Pluto\Validator();
    }
}
