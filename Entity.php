<?php

namespace Pluto;




abstract class Entity
{
    protected $id;
    protected $code;




    public function __construct()
    {
        $this->id = 0;
        $this->code = "";
    }




    public function SetId($id)
    {
        $this->id = $id;
    }

    public function Id()
    {
        return $this->id;
    }

    public function SetCode($code)
    {
        $this->code = $code;
    }

    public function Code()
    {
        return $this->code;
    }
}
