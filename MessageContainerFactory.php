<?php

namespace Pluto;




require_once(__DIR__ . "/MessageContainer.php");




class MessageContainerFactory
{
    public function __construct()
    {
    }




    public function Create()
    {
        return new \Pluto\MessageContainer();
    }
}
