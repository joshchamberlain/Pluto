<?php

namespace Pluto;




class MessageFactory
{
    public function __construct()
    {
    }




    public function Create($type, $code, $text, $processed)
    {
        return new \Pluto\Message($type, $code, $text, $processed);
    }
}
