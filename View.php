<?php

namespace Pluto;




require_once(__DIR__ . "/Config.php");




class View
{
    protected $baseDirectory;
    protected $baseUrl;
    protected $templateDirectory;

    protected $templates;
    protected $numTemplates;

    protected $data;




    public function __construct()
    {
        $this->baseDirectory = \Pluto\Config::Data("view_base_directory");
        $this->baseUrl = \Pluto\Config::Data("view_base_url");
        $this->templateDirectory = \Pluto\Config::Data("view_template_directory");

        $this->templates = array();
        $this->numTemplates = 0;

        $this->data = array();
        $this->data["baseDirectory"] = $this->baseDirectory;
        $this->data["baseUrl"] = $this->baseUrl;
        $this->data["templateDirectory"] = $this->templateDirectory;
    }




    public function Render()
    {
        // Easy data access.
        extract($this->data, EXTR_SKIP);

        $this->StartBuffering();

        for ($templateIterator = 0; $templateIterator < $this->numTemplates; $templateIterator++)
        {
            include($this->templateDirectory . $this->templates[$templateIterator] . ".php");
        }

        $this->StopBuffering();
    }




    public function AddTemplate($template)
    {
        $this->templates[$this->numTemplates] = $template;
        $this->numTemplates = $this->numTemplates + 1;
    }

    public function AddTemplates(Array $templates)
    {
        $numTemplates = sizeof($templates);

        for ($i = 0; $i < $numTemplates; $i++)
        {
            $this->AddTemplate($templates[$i]);
        }
    }

    public function RemoveTemplate($template)
    {
        for ($i = 0; $i < $this->numTemplates; $i++)
        {
            if ($this->templates[$i] === $template)
            {
                array_splice($this->templates, $i, 1);
            }
        }
    }




    public function AddData($key, $data)
    {
        $this->data[$key] = $data;
    }

    public function RemoveData($key)
    {
        unset($this->data[$key]);
    }

    public function Data($key)
    {
        $data = NULL;

        if (array_key_exists($key, $this->data))
        {
            $data = $this->data[$key];
        }

        return $data;
    }




    protected function Load($template)
    {
        include($this->templateDirectory . $template . ".php");
    }

    protected function StartBuffering()
    {
        ob_start();
    }

    protected function StopBuffering()
    {
        ob_end_flush();
    }

    protected function FlushBuffer()
    {
        return ob_get_flush();
    }

    protected function PrintErrorMessages($format, \Pluto\MessageContainer $messageContainer, Array $errorCodeMessages)
    {
        $errors = $messageContainer->ErrorMessages();
        $numErrors = $messageContainer->NumErrorMessages();

        for ($i = 0; $i < $numErrors; $i++)
        {
            if (array_key_exists($errors[$i]->Code(), $errorCodeMessages))
            {
                $errors[$i]->SetText($errorCodeMessages[$errors[$i]->Code()]);
            }
        }

        for ($i = 0; $i < $numErrors; $i++)
        {
            $o = $format;
            $o = str_replace("{type}", $errors[$i]->Type(), $o);
            $o = str_replace("{code}", $errors[$i]->Code(), $o);
            $o = str_replace("{text}", $errors[$i]->Text(), $o);
            $o = $o . "\n";

            echo $o;
        }
    }




    protected function Escape($dirty)
    {
        return htmlentities(strip_tags($dirty), ENT_QUOTES, "UTF-8");
    }
}
