<?php

namespace Pluto;




require_once(__DIR__ . "/Messenger.php");
require_once(__DIR__ . "/Validator.php");




abstract class Form extends \Pluto\Messenger
{
    protected $action;
    protected $successMessage;
    protected $errorMessage;
    protected $submitValue;

    protected $validator;




    public function __construct($action = "", $successMessage = "", $errorMessage = "", $submitValue = "")
    {
        parent::__construct();

        $this->action = $action;
        $this->successMessage = $successMessage;
        $this->errorMessage = $errorMessage;
        $this->submitValue = $submitValue;

        $this->validator = new \Pluto\Validator();
    }




    public function Validate()
    {
        return $this->ValidateData();
    }




    public function SetAction($action)
    {
        $this->action = $action;
    }

    public function Action()
    {
        return $this->action;
    }

    public function SetSuccessMessage($successMessage)
    {
        $this->successMessage = $successMessage;
    }

    public function SuccessMessage()
    {
        return $this->successMessage;
    }

    public function SetErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    public function ErrorMessage()
    {
        return $this->errorMessage;
    }

    public function SetSubmitValue($submitValue)
    {
        $this->submitValue = $submitValue;
    }

    public function SubmitValue()
    {
        return $this->submitValue;
    }




    abstract protected function ValidateData();
}
